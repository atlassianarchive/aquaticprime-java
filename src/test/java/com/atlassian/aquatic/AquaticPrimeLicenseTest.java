package com.atlassian.aquatic;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

/**
 * Copyright Atlassian: 17/11/11
 */
public class AquaticPrimeLicenseTest
{

    AquaticPrimeLicense license;

    @Before
    public void setup() {
        Map<String,String> properties = new HashMap<String, String>();
        properties.put("Product", "SourceTree");
        properties.put("Name", "Steve Smith");
        properties.put("Email", "ssmith@atlassian.com");

        license = new AquaticPrimeLicense(properties, "ABC".getBytes());
    }

    @After
    public void teardown() {

    }

    @Test
    public void testXml() {
        String xml = license.toXml();

        assertTrue(xml.contains("<plist version=\"1.0\">"));
        assertTrue(xml.contains("<key>Product</key>"));
        assertTrue(xml.contains("<string>SourceTree</string>"));
        assertTrue(xml.contains("<key>Email</key>"));
        assertTrue(xml.contains("<string>ssmith@atlassian.com</string>"));
        assertTrue(xml.contains("<key>Name</key>"));
        assertTrue(xml.contains("<string>Steve Smith</string>"));
        assertTrue(xml.contains("<data>ABC</data>"));

    }

    @Test
    public void testXmlEncoded() {
        Map<String,String> properties = new HashMap<String, String>();
        properties.put("Product", "SourceTree");
        properties.put("Name", "Steve & Smith");
        properties.put("Email", "steve&smith@atlassian.com");

        String xml = new AquaticPrimeLicense(properties, "ABC".getBytes()).toXml();

        assertTrue(xml.contains("<plist version=\"1.0\">"));
        assertTrue(xml.contains("<key>Product</key>"));
        assertTrue(xml.contains("<string>SourceTree</string>"));
        assertTrue(xml.contains("<key>Email</key>"));
        assertTrue(xml.contains("<string>steve&amp;smith@atlassian.com</string>"));
        assertTrue(xml.contains("<key>Name</key>"));
        assertTrue(xml.contains("<string>Steve &amp; Smith</string>"));
        assertTrue(xml.contains("<data>ABC</data>"));

    }


}
