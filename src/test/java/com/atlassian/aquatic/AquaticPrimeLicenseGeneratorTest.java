package com.atlassian.aquatic;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

/**
 * Copyright Atlassian: 17/11/11
 */
public class AquaticPrimeLicenseGeneratorTest
{

    AquaticPrimeLicenseGeneratorImpl generator;

    String pub = "C54C78CB9FFE121C4BCC17A7493B75D0C09591A6B5E2868CD85D8CC5F436B98FB308FBB39A701AE911D0E60A8744B43800AD5517FC202D84A1DF7FF60B9BE7D6AB7A084EA0D4F0F20CFEB778FF942E45613B6B8731EF5446A6B3A0BEFFD67CA6F8D52407F304D9D9709AE4BC1B770938037A5553E86DD4BD6754125B170F2CA7";
    String priv = "83885087BFFEB6BD87DD651A30D24E8B2B0E6119CE97045DE593B32EA2CF265FCCB0A7CD11A011F0B68B440704D8782555C8E36552C01E586BEA554EB267EFE3450A03549074035F06E1EEA9D687D9C40BA5077D05F55AB8E133E2126BC8504B325895E8F86CA91C7F02BD8A6437AF3AA91FE442689B586240D549982136828B";


    // Licenses generated with the above keys using AquaticPrime.php (included)
    String refLicense = "sfYjL1SXF4Fiq/tx6y4CrUNMP9mr1YrY2LlqhJpGeJTOcORpT/6KSRVWxa8JiUbub7soyGBjJKMS\r\n"+
                        "ruz+LudzCiBsajyGQ8853G3IsPOFXu/xsgquNuFSqzj30eVcXipaR9nc5qAiF0e/gH7sRtDZEIQT\r\n"+
                        "rTKhNztlPOcor8WfS30=\r\n";
    String refLicenseWithApos = "Iropy9T62uJ4VGathTygnq13LZ44JCI5+y1+jo0Abh02m6Od9qr+VSzaY+PMeyzbed3Fxeru28NS\r\n"+
                                "qjsAhPp4KwYMxjBdMPy7ofgL9TXA7cND/Car0dO8qg5j+H59lGwvjNyJwt9j4GSAe2eN9NrERRjJ\r\n"+
                                "AT6T0Tlsbia/F9nRhmQ=\r\n";
    String refLicenseWithMultApos = "WCwQnti6S3R+CrLzL5zOMtSOyNYvFaQh2QNFtJIqqjew6HCTHi3vAIGMBYpGyRDH7pjUqcs6pmgK\r\n"+
                                    "nQD0Lhb1ufW5d7MXFYH02WBBPHnjwwmQo3tM7io83bHbotmxw4ygxSOUsLub8JUFOP97vqUMcHvA\r\n"+
                                    "KQUyYBFn8aE2ajvSpXE=\r\n";


    @Before
    public void setup() {
    }

    @After
    public void teardown() {

    }

    @Test
    public void testReferenceLicense() throws Exception
    {
        generator = new AquaticPrimeLicenseGeneratorImpl(priv, pub);

        Map<String,String> properties = new HashMap<String, String>();
        properties.put("Product", "SourceTree");
        properties.put("Name", "Steve Smith");
        properties.put("Email", "ssmith@atlassian.com");

        AquaticPrimeLicense license = generator.generateLicense(properties);
        System.out.println(license.toXml());

        String licenseStr = new String(license.getData());
        assertEquals(refLicense, licenseStr);

    }

    @Test
    public void testReferenceLicenseWithApostrophes() throws Exception
    {
        generator = new AquaticPrimeLicenseGeneratorImpl(priv, pub);

        Map<String,String> properties = new HashMap<String, String>();
        properties.put("Product", "SourceTree");
        properties.put("Name", "Joe's Widgets");
        properties.put("Email", "joe@widgets.com");

        AquaticPrimeLicense license = generator.generateLicense(properties);

        String licenseStr = new String(license.getData());
        assertEquals(refLicenseWithApos, licenseStr);
    }

    @Test
    public void testReferenceLicenseWithMultipleApostrophes() throws Exception
    {
        generator = new AquaticPrimeLicenseGeneratorImpl(priv, pub);

        Map<String,String> properties = new HashMap<String, String>();
        properties.put("Product", "SourceTree");
        properties.put("Name", "Joe's Grammatically Incorrect Widget's");
        properties.put("Email", "joe@widgets.com");

        AquaticPrimeLicense license = generator.generateLicense(properties);
        System.out.println(license.toXml());

        String licenseStr = new String(license.getData());
        assertEquals(refLicenseWithMultApos, licenseStr);
    }


}
