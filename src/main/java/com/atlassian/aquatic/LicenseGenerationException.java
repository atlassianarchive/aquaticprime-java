package com.atlassian.aquatic;

/**
 * Wrapper exception for errors caught by the underlying implementation (usually crypto based)
 *
 * Copyright Atlassian: 18/11/11
 */
public class LicenseGenerationException extends Exception
{

    public LicenseGenerationException(String message)
    {
        super(message);
    }

    public LicenseGenerationException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public LicenseGenerationException(Throwable cause)
    {
        super(cause);
    }
}
