package com.atlassian.aquatic.util;

import com.atlassian.aquatic.AquaticPrimeLicense;
import com.atlassian.aquatic.AquaticPrimeLicenseGenerator;
import com.atlassian.aquatic.AquaticPrimeLicenseGeneratorImpl;
import com.atlassian.aquatic.LicenseGenerationException;
import jline.ConsoleReader;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * The following quick license generator can be run with:
 *
 *     mvn clean compile exec:exec
 *
 * You may need the production public and private key, these can be retrieved from HAMS.
 *
 * Copyright Atlassian: 25/01/12
 */
public class Console
{

    public static void main(String[] args) throws IOException
    {

        ConsoleReader reader = new ConsoleReader();

        String name = reader.readLine("Full Name> ");
        String email = reader.readLine("Email> ");
        String pubkey = reader.readLine("Public Key> ");
        String privkey = reader.readLine("Private Key> ");

        Map<String,String> properties = new HashMap<String, String>();
        properties.put("Product", "SourceTree");
        properties.put("Name", name);
        properties.put("Email", email);

        try {
            AquaticPrimeLicenseGenerator apGen = new AquaticPrimeLicenseGeneratorImpl(privkey, pubkey);
            AquaticPrimeLicense license = apGen.generateLicense(properties);

            System.out.println("License>");
            System.out.println(license.toXml());

        } catch (LicenseGenerationException e) {
            throw new RuntimeException(e);
        }

    }
}
