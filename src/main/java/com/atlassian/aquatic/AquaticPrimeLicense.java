package com.atlassian.aquatic;

import static org.apache.commons.lang.StringEscapeUtils.escapeXml;

import java.util.HashMap;
import java.util.Map;

/**
 * Basic wrapper class for an AquaticPrime license. It's main purpose is to convert the
 * generated license data into a Apple PList XML representation.
 *
 * See AquaticPrimeLicenseGeneratorImpl for the practical license implementation.
 *
 * Copyright Atlassian: 17/11/11
 */
public class AquaticPrimeLicense
{

    private Map<String,String> properties;
    private byte[] data;

    public AquaticPrimeLicense()
    {
        properties = new HashMap<String, String>();
    }

    public AquaticPrimeLicense(Map<String, String> properties, byte[] data)
    {
        this.properties = properties;
        this.data = data;
    }

    public Map<String, String> getProperties()
    {
        return properties;
    }

    public void setProperties(Map<String, String> properties)
    {
        this.properties = properties;
    }

    public void addProperty(String key, String value) {
        properties.put(key, value);
    }

    public byte[] getData()
    {
        return data;
    }

    public void setData(byte[] data)
    {
        this.data = data;
    }

    // Example license:
    //
    // <?xml version="1.0" encoding="UTF-8"?>
    // <!DOCTYPE plist PUBLIC "-//Apple Computer//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
    // <plist version="1.0">
    // <dict>
    //        <key>Product</key>
    //        <string>SourceTree</string>
    //        <key>Name</key>
    //        <string>Steve Smith</string>
    //        <key>Email</key>
    //        <string>ssmith@atlassian.com</string>
    //        <key>Signature</key>
    //        <data>sfYjL1SXF4Fiq/tx6y4CrUNMP9mr1YrY2LlqhJpGeJTOcORpT/6KSRVWxa8JiUbub7soyGBjJKMS
    // ruz+LudzCiBsajyGQ8853G3IsPOFXu/xsgquNuFSqzj30eVcXipaR9nc5qAiF0e/gH7sRtDZEIQT
    // rTKhNztlPOcor8WfS30=
    // </data>
    // </dict>
    // </plist>
    public String toXml() {
        // We may be able to do this with JAXB but that would be overkill...
        StringBuilder xml = new StringBuilder("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n")
            .append("<!DOCTYPE plist PUBLIC \"-//Apple Computer//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">\n")
            .append("<plist version=\"1.0\">\n")
            .append("<dict>\n");

        for (String key : properties.keySet()) {
            xml.append(String.format("\t<key>%s</key>\n", escapeXml(key)));
            xml.append(String.format("\t<string>%s</string>\n", escapeXml(properties.get(key))));
        }
        xml.append(String.format("\t<key>Signature</key>\n"));
        xml.append(String.format("\t<data>%s</data>\n", new String(data)));

        xml.append("</dict>\n</plist>\n");

        return xml.toString();
    }
}
