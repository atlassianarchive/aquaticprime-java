package com.atlassian.aquatic;

import java.util.Map;

/**
 * Copyright Atlassian: 17/11/11
 */
public interface AquaticPrimeLicenseGenerator
{

    AquaticPrimeLicense generateLicense(Map<String, String> properties)
        throws LicenseGenerationException;

}
