# AquaticPrime-java

## Overview

This is a Java implementation of the AquaticPrime license generator, based on the PHP/Python modules.

## Example Usage

See AquaticPrimeLicenseGeneratorTest for usage examples.

## Commandline usage

This library may be used from the commandline to generate licenses. To use it do:

    mvn clean compile exec:exec

## License

Apache License 2.0: See LICENCE file for details.
